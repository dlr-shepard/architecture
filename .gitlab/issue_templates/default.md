# Architecture Proposal

<!--
Thanks for contributing to our project!
Before creating an architecture proposal, please read this first:

- Use the template below
- Check if there are related issues
- Determine which repository the request should be reported in:
  https://gitlab.com/dlr-shepard/architecture/-/blob/main/CONTRIBUTING.md#how-can-i-contribute
- Use a clear and descriptive title for the issue to identify the suggestion
- For code snippets and logs please use Markdown code blocks:
  https://docs.gitlab.com/ee/user/markdown.html

-->

## Describe your proposal

<!--
Describe the proposal with as many details as possible
-->

### Improvement

<!--
What is the benefit and why is it interesting for all users?
-->

### Consequences

<!--
What are the consequences of your proposal?
-->

## Related Issues

- #<issue number>
