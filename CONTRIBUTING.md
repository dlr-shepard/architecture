# Contributing to shepard

First off, thanks for taking time to contribute!
The following is a set of guidelines for contributing to the shepard architecture.
These are mostly guidelines, not rules.
Use your best judgment, and feel free to propose changes to this document in a merge request.

Please note we have a code of conduct, please follow it in all your interactions with the project.

## Table of Contents

- [Code of Conduct](#code-of-conduct)
- [I don't want to read this whole thing I just have a question!](#i-dont-want-to-read-this-whole-thing-i-just-have-a-question)
- [How Can I Contribute?](#how-can-i-contribute)

## Code of Conduct

Everyone participating in this project is asked to conduct themselves in a reasonable and proper manner.
All interactions should be respectful and friendly, as is appropriate in a professional setting.
By participating, you are accepting this CoC.

## I don't want to read this whole thing I just have a question!

Please don't file an issue to ask a question.
You'll get faster results by contacting us on [Mattermost at HZDR](https://mattermost.hzdr.de/signup_user_complete/?id=f5ycfi3nmigixxaerhpdg6q66y)

## How Can I Contribute?

### Issue Boards and Labels

We use issue [labels](https://docs.gitlab.com/ee/user/project/labels.html) and [boards](https://docs.gitlab.com/ee/user/project/issue_board.html) to manage ongoing issues.

If you have a proposal to change things in Shepard that affect the entire ecosystem, you should submit an issue [in this repository](https://gitlab.com/dlr-shepard/architecture/-/issues).
Please provide as much detail as possible by filling in this [template](.gitlab/issue_templates/default.md).
This will help the upcoming discussion.

Once the discussion has reached a conclusion, you can create a merge request submitting your proposed changes to the concepts.

### Merge Request Process

This section guides you through submitting a merge request for the shepard architecture.
Following these guidelines helps maintainers to review your merge request faster.

First, create a new branch.
You can create a branch within the architecture repository if you have the necessary permissions.
If not, you can always fork the repository and create a new branch there.
Now you are ready to begin with your contribution.

Once you have created an initial prototype of your contribution, commit your changes to GitLab and open a merge request with a meaningful name.
Please fill in the required [template](.gitlab/merge_request_templates/default.md) and link the merge request to the preceding issue.

After your first commit, you can add as many additional commits as you like.
After your contribution is accepted, your changes will be squashed into one commit anyway.

While you are working on your contribution, others may merge their merge request.
To keep up with current developments, it is a good idea to occasionally [rebase](https://docs.gitlab.com/ee/topics/git/git_rebase.html#git-rebase) your branch to the current main branch.
