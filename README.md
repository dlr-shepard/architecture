# Architecture

> This project has been moved to the [Mono Repository](https://gitlab.com/dlr-shepard/shepard/-/tree/main/docs/architecture) and is now read-only.

General architecture documents and discussions are managed in this repository.
For more information about shepard, its usage and infrastructure, check out [the wiki](https://gitlab.com/dlr-shepard/documentation/-/wikis/home).

Major conceptual changes should be discussed here before they are implemented.
Feature requests can be submitted via an issue.
Discussion under an issue may or may not lead to an idea of how that feature could be implemented.
Once this idea grows into something substantial, a merge request can be submitted outlining the changes to be made.
The changes can be implemented once the Merge Request has been approved and merged.

## The purpose of this repository

- Documentation of the design decision
- Discussion in the form of merge requests
- Future features in the form of issues

## Content of this repository

- General architecture descriptions
- A confirmed roadmap
- Proposals

## What does not belong here

- Brainstoring (use issues instead)
- User documentation (the [documentation repository](https://gitlab.com/dlr-shepard/documentation))
